<?php

namespace ProdCalcApiCommons;

class Config
{

    // public const EP = [

    //     'accountsApi' => 'http://tst-accounts.prodcalc.com:80',

    //     'globalDataApi' => 'http://tst-globaldata.prodcalc.com:80',

    //     'communicationsApi' => 'http://tst-communications.prodcalc.com:80',

    //     'mrpApi' => 'http://tst-mrp.prodcalc.com:80'

    // ];

    public const EP = [

        'accountsApi' => 'http://local-accounts.prodcalc.com:80',

        'globalDataApi' => 'http://local-globaldata.prodcalc.com:80',

        'communicationsApi' => 'http://local-communications.prodcalc.com:80',

        'mrpApi' => 'http://local-mrp.prodcalc.com:80'

    ];
}
