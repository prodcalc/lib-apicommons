<?php

namespace ProdCalcApiCommons;

class RegExp
{

	public const NAME						= '/^([a-z][a-z0-9]+)$/i';
	public const PERSON_QUALIFIED_NAME		= '/^([a-z][a-z0-9]+)\.([a-z][a-z0-9]+)\.([0-9]+)$/i';
	public const BUSINESS_QUALIFIED_NAME	= '/^([a-z][a-z0-9]+)\.([0-9]+)$/i';

	public const EMAIL			= '/^.+@.+(\\..+)+$/i';
	public const EMAIL_SUBJECT	= '/^.{1,50}$/i';
	public const EMAIL_BODY		= '/^.{1,2000}$/i';
	public const EMAIL_TOKEN	= '/^[a-z0-9]{1,12}$/i';

	public const ACCOUNT_TOKEN	= '/^[a-z0-9]{12}$/i';
	public const BUSINESS_TOKEN	= '/^[a-z0-9]{12}$/i';
	public const BASIC_TOKEN	= '/^[a-z0-9]{12}$/i';

	public const PAGE_CONTENT	= '/^.{0,20000}$/i';

	public const URL			= '/^.+$/i';
	public const TEXT			= '/^.+$/i';
	public const PROFILE_TEXT	= '/^(.+){1,45}$/i';
	public const DATE			= '/^(\d{4})-(\d{2})-(\d{2})$/i';
	public const INT			= '/^(\-?\d+)$/i';
}
