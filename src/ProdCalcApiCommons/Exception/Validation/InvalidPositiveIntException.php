<?php

namespace ProdCalcApiCommons\Exception\Validation;

use ProdCalcFramework\Exception\BasicException;

class InvalidPositiveIntException extends BasicException
{
}
