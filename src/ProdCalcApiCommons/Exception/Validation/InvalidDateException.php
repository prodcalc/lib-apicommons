<?php

namespace ProdCalcApiCommons\Exception\Validation;

use ProdCalcFramework\Exception\BasicException;

class InvalidDateException extends BasicException
{
}
