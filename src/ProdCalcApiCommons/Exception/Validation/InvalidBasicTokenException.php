<?php

namespace ProdCalcApiCommons\Exception\Validation;

use ProdCalcFramework\Exception\BasicException;

class InvalidBasicTokenException extends BasicException
{
}
