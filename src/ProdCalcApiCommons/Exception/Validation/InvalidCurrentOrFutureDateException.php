<?php

namespace ProdCalcApiCommons\Exception\Validation;

use ProdCalcFramework\Exception\BasicException;

class InvalidCurrentOrFutureDateException extends BasicException
{
}
