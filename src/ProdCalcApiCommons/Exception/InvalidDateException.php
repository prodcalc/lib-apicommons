<?php

namespace ProdCalcFramework\Exception;

use ProdCalcFramework\Exception\BasicException;

// @deprecated
class InvalidDateException extends BasicException
{
}
