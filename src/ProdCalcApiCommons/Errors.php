<?php

namespace ProdCalcApiCommons;

class Errors
{
    public const SESSION_TOKENNOTFOUND     = 'SESSION_TOKENNOTFOUND';
    public const PERSON_NOTFOUND           = 'PERSON_NOTFOUND';
    public const BUSINESS_NOTFOUND         = 'BUSINESS_NOTFOUND';
    public const ACCOUNT_NOTFOUND          = 'ACCOUNT_NOTFOUND';
}
