<?php

namespace ProdCalcApiCommons;

use ProdCalcApiCommons\Config;

class Constants
{
    public const DB_CONFIG_OPTIONS = [

        'options'      =>  [

            \PDO::ATTR_ERRMODE              => \PDO::ERRMODE_EXCEPTION,

            \PDO::ATTR_DEFAULT_FETCH_MODE   => \PDO::FETCH_ASSOC,

            \PDO::ATTR_EMULATE_PREPARES     => false

        ]

    ];

    public const ACCOUNTSAPI_SERVER = Config::EP['accountsApi'];
    public const GLOBALDATAAPI_SERVER = Config::EP['globalDataApi'];
    public const COMMUNICATIONSAPI_SERVER = Config::EP['communicationsApi'];
    public const MRPAPI_SERVER = Config::EP['mrpApi'];

    public const FROM_EMAIL = 'no-reply@prodcalc.com';

    public const SESSION_ID     = 'Phpsessionid';

    public const CONFIG_ID     = 'COMPRODCALC_CONFIG';
}
