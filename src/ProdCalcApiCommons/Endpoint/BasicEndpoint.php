<?php

namespace ProdCalcApiCommons\Endpoint;

use ProdCalcApiCommons\Constants as LIBC;

use ProdCalcApiCommons\Errors;
use ProdCalcFramework\Constants as FMKC;
use ProdCalcFramework\Endpoint\BasicEndpoint as FMKBE;

use ProdCalcFramework\Service\Http\HttpService;

use ProdCalcFramework\Service\Db\DbConnection;
use ProdCalcFramework\Service\Db\DbService;

use ProdCalcFramework\Exception\InternalServerErrorException;
use ProdCalcFramework\Exception\Db\TransactionException;
use ProdCalcFramework\Exception\Http\HttpException;
use ProdCalcMRPApi\Service\Calendar\CalendarService;
use ProdCalcMRPApi\Service\Material\MaterialService;

abstract class BasicEndpoint extends FMKBE
{

    private $sessionDetail;
    private $logger;

    function run()
    {

        $t = $this;

        $t

            ->setDbService(

                (new DBService())->setEndpoint($t)

            );

        try {

            $t->getDbService()->setDbConnection(

                (new DbConnection())

                    ->connect(

                        $t->getDbConfig()

                    )->getConnection()

            );
        } catch (TransactionException $ex) {

            throw new InternalServerErrorException($ex->getMessage());
        }
    }

    function getEnv(

        $k // config key

    ) {

        return $_ENV[LIBC::CONFIG_ID][$k];
    }

    function getDate(

        $dateTime

    ) {

        return substr($dateTime, 0, 10);
    }

    function addError(

        $message

    ) {

        $t = $this;

        $t

            ->getErrors()
            ->addError(

                $message

            );

        return $t;
    }

    function throwInvalidInputException()
    {

        $t = $this;

        $t

            ->getErrors()
            ->throwInvalidInputException();

        return $t;
    }

    function getField(

        $fieldName

    ) {

        $t = $this;

        return $t

            ->getRequest()
            ->getField(

                $fieldName

            );
    }

    function getBody()
    {

        $t = $this;

        return $t

            ->getRequest()
            ->getBody();
    }

    function setBody(

        $body

    ) {

        $t = $this;

        return $t

            ->getRequest()
            ->setBody(

                $body

            );
    }

    function setErrorList(

        $errorList

    ) {

        $t = $this;

        return $t

            ->getErrors()
            ->setErrorList(

                $errorList

            );

        return $t;
    }

    function warning(

        $message

    ) {

        $t = $this;

        $t

            ->logger
            ->warning(

                $message

            );

        return $t;
    }

    abstract function getDbConfig();

    function hasPermission(

        $businessToken,

        $permissionList
    ) {

        $t = $this;

        $permission = $t->permissionByBusinessToken(

            $businessToken

        );

        if (in_array($permission, $permissionList)) {

            return;
        }

        $t->getErrors()->add('no permission');

        $t->throwTransactionException();
    }

    function permissionByBusinessToken(

        $businessToken

    ) {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($t);

        $httpService

            ->getRequest()
            ->setEndpoint(

                $t->getEnv('accountsApi') .

                    '/permission/permissionByBusinessToken/' .

                    $businessToken

            );

        $httpService

            ->getRequest()
            ->setHeaders(

                $t

                    ->getRequest()
                    ->getHeaders()

            );

        $t->checkHttpStatus(

            $httpService

        );

        return $httpService

            ->get()
            ->getResponse()
            ->getBody()['code'];
    }

    function materialByToken(

        $token

    ) {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($t);

        $httpService

            ->getRequest()
            ->setEndpoint(

                $t->getEnv('mrpApi') .

                    '/material/materialProfileByToken/' .

                    $token

            );

        $httpService

            ->getRequest()
            ->setHeaders(

                $t

                    ->getRequest()
                    ->getHeaders()

            );

        $t->checkHttpStatus(

            $httpService

        );

        return $httpService

            ->get()
            ->getResponse()
            ->getBody();
    }

    function checkHttpStatus(

        $httpService

    ) {

        $t = $this;

        if (
            $httpService

            ->get()
            ->getResponse() === 400
        ) {

            $t->getErrors()->add('no permission');

            $t->throwTransactionException();
        } else if (

            $httpService

            ->get()
            ->getResponse() === 500

        ) {

            $t->throwInternalServerErrorException(

                new \Exception('no connection to external endpoint')

            );
        };
    }

    public function getPersonQualifiedNameFromSession()
    {

        $t = $this;

        return $t->buildPersonQualifiedName($t->getSessionDetail()['person']);
    }

    public function buildPersonQualifiedName($person)
    {

        return

            $person['firstName'] . '.' .
            $person['lastName'] . '.' .
            $person['counter'];
    }

    public function getPersonQualifiedName($accountToken)
    {

        $person = $this->getPersonProfileByAccountToken($accountToken);

        return

            $person['firstName'] . '.' .
            $person['lastName'] . '.' .
            $person['counter'];
    }

    public function getPersonProfileByAccountToken($accountToken)
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($t);

        $httpService

            ->getRequest()
            ->setEndpoint(

                $t->getEnv('accountsApi') .

                    '/person/personProfileByAccountToken' .

                    '/' . $accountToken


            )
            ->setHeaders(

                $t

                    ->getRequest()
                    ->getHeaders()

            );

        try {

            $httpService->get();
        } catch (HttpException $ex) {

            $t

                ->getErrors()
                ->add($ex->getMessage());

            $t->throwTransactionException();
        }

        $res = $httpService->getResponse();

        if ($res->getHttpStatus() !== FMKC::HTTP_OK) {

            $t

                ->getErrors()
                ->add(Errors::PERSON_NOTFOUND);

            $t->throwTransactionException();
        }

        return $res->getBody();
    }

    public function getPersonProfileByQualifiedName($personQualifiedName)
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($t);

        $httpService

            ->getRequest()
            ->setEndpoint(

                // LIBC::ACCOUNTSAPI_SERVER .

                $t->getEnv('accountsApi') .

                    '/person/personProfileByQualifiedName' .

                    '/' . $personQualifiedName


            )
            ->setHeaders(

                $t

                    ->getRequest()
                    ->getHeaders()

            );

        try {

            $httpService->get();
        } catch (HttpException $ex) {

            $t

                ->getErrors()
                ->add($ex->getMessage());

            $t->throwTransactionException();
        }

        $res = $httpService->getResponse();

        if ($res->getHttpStatus() !== FMKC::HTTP_OK) {

            $t

                ->getErrors()
                ->add(Errors::PERSON_NOTFOUND);

            $t->throwTransactionException();
        }

        return $res->getBody();
    }

    public function setSessionDetail()
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($t);

        $httpService

            ->getRequest()
            ->setEndpoint(

                $t->getEnv('accountsApi') .

                    '/session/sessionDetail'

            )
            ->setHeaders(

                $t

                    ->getRequest()
                    ->getHeaders()

            );

        try {

            $httpService->get();
        } catch (HttpException $ex) {

            $t

                ->getErrors()
                ->add(Errors::SESSION_TOKENNOTFOUND);

            $t->throwTransactionException();
        }

        $res = $httpService->getResponse();

        if ($res->getHttpStatus() !== FMKC::HTTP_OK) {

            $t

                ->getErrors()
                ->add(Errors::SESSION_TOKENNOTFOUND);

            $t->throwTransactionException();
        }

        $t->sessionDetail = $res->getBody();

        return $this;
    }

    public function getSessionDetail()
    {

        return $this->sessionDetail;
    }

    public function getAccountToken()
    {

        return $this->getSessionDetail()['account']['token'];
    }

    public function getPersonAccountToken($personQualifiedName)
    {

        return $this->getPersonProfileByQualifiedName($personQualifiedName)['account']['token'];
    }

    public function sendHtmlEmail($data)
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($this);

        $httpService

            ->getRequest()
            ->setEndpoint(

                // LIBC::COMMUNICATIONSAPI_SERVER .

                $t->getEnv('communicationsApi') .

                    '/email/sendHTMLEmail'

            )
            ->setBody(

                $data

            );

        $httpService->post();
    }

    public function getCurrentDateTime()
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($this);

        $httpService

            ->getRequest()
            ->setEndpoint(

                // LIBC::GLOBALDATAAPI_SERVER .

                $t->getEnv('globalDataApi') .

                    '/calendar/getCurrentDateTime'

            );

        return $httpService

            ->get()
            ->getResponse()
            ->getBody()['currentDateTime'];
    }

    public function getCurrentDate()
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($this);

        $httpService

            ->getRequest()
            ->setEndpoint(

                // LIBC::GLOBALDATAAPI_SERVER .

                $t->getEnv('globalDataApi') .

                    '/calendar/getCurrentDate'

            );

        return $httpService

            ->get()
            ->getResponse()
            ->getBody()['currentDate'];
    }

    public function getTreatedCurrentDateTime()
    {

        $t = $this;

        try {

            return $t->getCurrentDateTime();
        } catch (HttpException $ex) {

            $t->throwInternalServerErrorException($ex);
        }
    }

    public function getTreatedCurrentDate()
    {

        $t = $this;

        try {

            return $t->getCurrentDate();
        } catch (HttpException $ex) {

            $t->throwInternalServerErrorException($ex);
        }
    }

    public function getBasicToken()
    {

        $t = $this;

        $httpService = (new HttpService())->setEndpoint($this);

        $httpService

            ->getRequest()
            ->setEndpoint(

                // LIBC::GLOBALDATAAPI_SERVER .

                $t->getEnv('globalDataApi') .

                    '/security/getBasicToken'

            );

        return $httpService

            ->get()
            ->getResponse()
            ->getBody()['basicToken'];
    }

    public function getTreatedBasicToken()
    {

        $t = $this;

        try {

            return $t->getBasicToken();
        } catch (HttpException $ex) {

            $t->throwInternalServerErrorException($ex);
        }
    }

    public function getMaterialService()
    {

        $t = $this;

        return (new MaterialService())->setEndpoint($t);
    }

    public function getCalendarService()
    {

        $t = $this;

        return (new CalendarService())->setEndpoint($t);
    }

    public function getMaterialByToken(

        $materialToken

    ) {

        $t = $this;

        return $t

            ->getMaterialService()
            ->findMaterialByToken(

                $materialToken

            );
    }

    public function getMaterialIdByToken(

        $materialToken

    ) {

        $t = $this;

        return $t->getMaterialIdByToken(

            $materialToken

        )['id'];
    }
}
